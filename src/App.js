import BarraNavegacion from "./BarraNavegacion";
import React from "react";
import { Row, Container } from "react-bootstrap";
import TablaMotos from "./TablaMotos";
import MenuVertical from "./MenuVertical";

function App() {

  const array_motos = [
    {
        "model": "YAMAHA X-MAX 250",
        "preu": 1300,
        "kilometres": 68000,
        "cilindrada": 249,
        "lloc": "Barcelona",
        "any": 2007
    },
    {
        "model": "SUZUKI BURGMAN 200",
        "preu": 1200,
        "kilometres": 45000,
        "cilindrada": 200,
        "lloc": "Barcelona",
        "any": 2007
    },
    {
        "model": "HONDA FORZA 250 X",
        "preu": 1599,
        "kilometres": 15000,
        "cilindrada": 250,
        "lloc": "Barcelona",
        "any": 2007
    },
    {
        "model": "HONDA SCOOPY SH150i",
        "preu": 1600,
        "kilometres": 49000,
        "cilindrada": 152,
        "lloc": "Barcelona",
        "any": 2006
    },
    {
        "model": "HONDA SCOOPY SH150i",
        "preu": 1400,
        "kilometres": 54000,
        "cilindrada": 152,
        "lloc": "Barcelona",
        "any": 2006
    }
];

  return (
    <div className="App">
      <BarraNavegacion />

      <Container>
        <Row>
          <div className="col-4">
            <MenuVertical />
          </div>
          <div className="col-7">
            <h1 className="m-5">PIAGGIO</h1>
            <TablaMotos datos={array_motos}/>
            {/* no se le puede dar margenes  directamente al jsx. */}
          </div>
        </Row>
      </Container>
    </div>
  );
}

export default App;
