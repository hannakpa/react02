import Table from 'react-bootstrap/Table';




function transformacion(moto, i) {
    return (
        <tr key={i}>
            {/* VA MOSTRANDO LOS INDICES. */}
            <td>{i+1}</td> 
            <td>{moto.model}</td>
            <td>{moto.preu}</td>
            <td>{moto.kilometres}</td>
            <td>{moto.cilindrada}</td>
            <td>{moto.lloc}</td>
            <td>{moto.any}</td>
        </tr>
    )
}

// -----///////////////////////////////////////////
function TablaMotos(props) {
let lista=props.datos;

let filas_de_tabla = lista.map(transformacion);
// ESCRIBIR UN BUCLE PARA LA SUMA
const sumaTotal = lista.map(item => item.preu).reduce((prev, curr) => prev + curr, 0); //donde pongo la key{i}??

    return (
        <Table responsive>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Model</th>
                    <th>Preu</th>
                    <th>Kilometres</th>
                    <th>Cilindrada</th>
                    <th>Lloc</th>
                    <th>Any</th>
                </tr>
            </thead>
            <tbody>
                {filas_de_tabla}
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <th><h4>Total:</h4></th>
                    <th>{sumaTotal}</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
            </tfoot>
        </Table>
    )

}

export default TablaMotos